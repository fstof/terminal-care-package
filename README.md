## Clone the repo into `$HOME`
```bash
git clone --recurse-submodules git@gitlab.com:fstof/terminal-care-package.git $HOME/terminal-care-package
```

## CD into folder
```bash
cd $HOME/terminal-care-package
```

## Execute `setup.sh`
```bash
./setup.sh
```